#!/bin/python3

import sys

def gridSearch(G, P):
  gridRows = len(G)
  gridCols = len(G[0])
  lookUpRows = len(P)
  lookUpCols = len(P[0])
  
  # Only need to look up to the "lower bound" range of the pattern size
  rowUpperBound = gridRows - lookUpRows + 1
  colUpperBound = gridCols - lookUpCols + 1

  for x in range(0, rowUpperBound):
    for y in range(0, colUpperBound):
      isMatch = True    # Use Bool flag to keep track
      for m in range(0, lookUpRows):
        for n in range(0, lookUpCols):
          if (G[x + m][y + n] != P[m][n]):
            isMatch = False
            break   # Break out of `n` loop

        if not isMatch:
          break     # Break out of `m` loop
      
      if isMatch:
        return "YES"

  return "NO"

if __name__ == "__main__":
  t = int(input().strip())
  for a0 in range(t):
    R, C = input().strip().split(' ')
    R, C = [int(R), int(C)]
    G = []
    G_i = 0
    for G_i in range(R):
      G_t = str(input().strip())
      G.append(G_t)
    r, c = input().strip().split(' ')
    r, c = [int(r), int(c)]
    P = []
    P_i = 0
    for P_i in range(r):
      P_t = str(input().strip())
      P.append(P_t)
    result = gridSearch(G, P)
    print(result)
