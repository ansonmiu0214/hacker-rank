process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
  input_stdin += data;
});

process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  main();
});

function readLine() {
  return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function gridSearch(G, P) {
  const gridRows = G.length;
  const gridCols = G[0].length;
  const lookUpRows = P.length;
  const lookUpCols = P[0].length;

  const rowUpperBound = gridRows - lookUpRows;
  const colUpperBound = gridCols - lookUpCols;

  for (var x = 0; x <= rowUpperBound; ++x) {
    for (var y = 0; y <= colUpperBound; ++y) {
      var isMatch = true;
      for (var m = 0; m < lookUpRows; ++m) {
        for (var n = 0; n < lookUpCols; ++n) {
          if (G[x + m][y + n] !== P[m][n]) {
            isMatch = false;
            break;
          }
        }

        if (!isMatch) break;
      }
      if (isMatch) return "YES";
    }
  }
  return "NO";
}

function main() {
  var t = parseInt(readLine());
  for (var a0 = 0; a0 < t; a0++) {
    var R_temp = readLine().split(' ');
    var R = parseInt(R_temp[0]);
    var C = parseInt(R_temp[1]);
    var G = [];
    for (var G_i = 0; G_i < R; G_i++) {
      G[G_i] = readLine();
    }
    var r_temp = readLine().split(' ');
    var r = parseInt(r_temp[0]);
    var c = parseInt(r_temp[1]);
    var P = [];
    for (var P_i = 0; P_i < r; P_i++) {
      P[P_i] = readLine();
    }
    var result = gridSearch(G, P);
    process.stdout.write("" + result + "\n");
  }
}

// Export methods for unit testing
var methods = {};
methods.gridSearch = gridSearch;
module.exports = methods;
