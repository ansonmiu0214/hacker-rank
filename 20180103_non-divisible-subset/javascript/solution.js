process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function(data) {
  input_stdin += data;
});

process.stdin.on('end', function() {
  input_stdin_array = input_stdin.split("\n");
  main();
});

function readLine() {
  return input_stdin_array[input_currentline++];
}

function nonDivisibleSubset(k, arr) {
  var remainder = [];
  var count = 0;
  for (var i = 0; i < k; ++i) remainder.push(0);

  for (num in arr) ++remainder[arr[num] % k];

  var upperBound = Math.floor(k / 2) + 1;
  for (var i = 0; i < upperBound; ++i) {
    var pair = (k - i) % k;

    if (i == pair || i == 0) {
      if (remainder[i] > 0) ++count;
    } else {
      count += Math.max(remainder[i], remainder[pair]);
    }
  }

  return count;
}

function main() {
  var n_temp = readLine().split(' ');
  var n = parseInt(n_temp[0]);
  var k = parseInt(n_temp[1]);
  arr = readLine().split(' ');
  arr = arr.map(Number);
  var result = nonDivisibleSubset(k, arr);
  process.stdout.write("" + result + "\n");
}

var methods = {};
methods.nonDivisibleSubset = nonDivisibleSubset;
module.exports = methods;