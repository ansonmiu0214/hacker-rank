var chai = require('chai');   // assertion library
var expect = chai.expect;
var solution = require('./../solution.js');

describe('Solution', function() {
  it('sample test case 0', function() {
    var k = 3;
    var arr = [1,7,4,2];
    var exp = 3;
    expect(solution.nonDivisibleSubset(k, arr)).to.equal(exp);
  });

  it('sample test case 2', function() {
    var k = 9;
    var arr = [422346306, 940894801, 696810740, 862741861, 85835055, 313720373];
    var exp = 5;
    expect(solution.nonDivisibleSubset(k, arr)).to.equal(exp);
  });
});