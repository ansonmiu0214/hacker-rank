import unittest, solution

testcases = [(3, [1,7,2,4], 3)
            ,(9, [422346306, 940894801, 696810740, 862741861, 85835055, 313720373], 5)
            ]

class TestSolution(unittest.TestCase):

  def test_samplecase(self):
    for (k, arr, exp) in testcases:
      self.assertEqual(solution.nonDivisibleSubset(k, arr), exp)

if __name__ == '__main__':
  unittest.main()