#!/bin/python3

import sys

def nonDivisibleSubset(k, arr):
    remainder = [0] * k

    # Categorise numbers by their remainders
    for number in arr:
      remainder[number % k] += 1
    
    # Iterate through remainders
    count = 0
    upperBound = k // 2 + 1
    for i in range(0, upperBound):
      pair = (k - i) % k
      if i == pair or i == 0:
        # Can only include at most 1 number (since mods of 2+ numbers in this sum to 0)
        count += remainder[i] > 0
      else:
        # Take the larger of the 2 remainder sets
        count += max(remainder[i], remainder[pair])

    return count

if __name__ == "__main__":
  n, k = input().strip().split(' ')                 # [n, k]
  n, k = [int(n), int(k)]                           # map to int
  arr = list(map(int, input().strip().split(' ')))
  result = nonDivisibleSubset(k, arr)
  print(result)
