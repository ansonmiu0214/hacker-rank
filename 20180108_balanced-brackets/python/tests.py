#!/bin/python3

import unittest, solution

test_case_1 = [("{[()]}", True)
              ,("{[(])}", False)
              ,("{{[[(())]]}}", True)
              ]

test_case_2 = [("}][}}(}][))]", False)
              ,("[](){()}", True)
              ,("()", True)
              ,("({}([][]))[]()", True)
              ,("{)[](}]}]}))}(())(", False)
              ,("([[)", False)
              ]

class BalancedBracketTests(unittest.TestCase):
  def test_case_1(self):
    for (text, exp) in test_case_1:
      self.assertEqual(solution.check_brackets(text), exp)

  def test_case_2(self):
    for (text, exp) in test_case_2:
      self.assertEqual(solution.check_brackets(text), exp)


if __name__ == "__main__":
  unittest.main()