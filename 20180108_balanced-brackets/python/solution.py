#!/bin/python3

import sys
from collections import deque

# Pre: each char in text is in the set "()[]{}"
def check_brackets(text):
  stack = deque()
  closing = { ')' : '(', '}' : '{', ']' : '[' }

  for bracket in text:
    if bracket in closing:
      if len(stack) == 0:
        return False
      if closing[bracket] != stack.pop():
        return False
    else:
      stack.append(bracket)
  
  return len(stack) == 0


if __name__ == "__main__":
  num_of_tests = int(input().strip())
  for i in range(num_of_tests):
    test_case = list(input().strip())
    result = check_brackets(test_case)
    print("YES" if result else "NO")