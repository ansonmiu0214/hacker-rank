"use strict";

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  main();
});

function readLine() {
  return input_stdin_array[input_currentline++];
}

function isBalanced(text) {
  var text_arr = Array.from(text);
  var closing = {
    ')': '(',
    ']': '[',
    '}': '{'
  };
  var stack = [];

  for (var i in text) {
    var bracket = text[i];
    if (bracket in closing) {
      if (stack.length == 0) return false;
      var prev_bracket = stack.pop();
      var opening = closing[bracket];
      if (prev_bracket !== opening) return false;
    } else {
      stack.push(bracket);
    }
  }

  return stack.length == 0;
}

function main() {
  var n = parseInt(readLine());
  for (var i = 0; i < n; ++i) {
    var text = readLine();
    var result = isBalanced(text);
    process.stdout.write((result ? "YES" : "NO") + "\n");
  }
}

// Make function be available for unit testing
module.exports = { isBalanced: isBalanced };