'use strict';

var chai = require('chai');
var expect = chai.expect;
var Solution = require('./../Solution');

describe('Solution', function () {
  it('test-0', function () {
    var tests = [["{[()]}", true], ["{[(])}", false], ["{{[[(())]]}}", true]];

    tests.map(function (test) {
      return expect(Solution.isBalanced(test[0])).to.equal(test[1]);
    });
  });

  it('test-1', function () {
    var tests = [["}][}}(}][))]", false], ["[](){()}", true], ["()", true], ["({}([][]))[]()", true], ["{)[](}]}]}))}(())(", false], ["([[)", false]];

    tests.map(function (test) {
      return expect(Solution.isBalanced(test[0])).to.equal(test[1]);
    });
  });
});