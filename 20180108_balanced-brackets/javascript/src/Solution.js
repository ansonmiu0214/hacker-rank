process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentline = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split("\n")
  main()
})

function readLine() {
  return input_stdin_array[input_currentline++]
}

function isBalanced(text) {
  const text_arr = Array.from(text)
  const closing = {
    ')' : '(',
    ']' : '[',
    '}' : '{'
  }
  let stack = []

  for (let i in text) {
    const bracket = text[i]
    if (bracket in closing) {
      if (stack.length == 0) return false
      const prev_bracket = stack.pop()
      const opening = closing[bracket]
      if (prev_bracket !== opening) return false
    } else {
      stack.push(bracket)
    }
  }

  return stack.length == 0
}

function main() {
  const n = parseInt(readLine())
  for (let i = 0; i < n; ++i) {
    const text = readLine()
    const result = isBalanced(text)
    process.stdout.write(`${result ? "YES" : "NO"}\n`)
  }
}

// Make function be available for unit testing
module.exports = { isBalanced: isBalanced }