const chai = require('chai')
const expect = chai.expect
const Solution = require('./../Solution')

describe('Solution', () => {
  it('test-0', () => {
    const tests = [["{[()]}", true]
                        ,["{[(])}", false]
                        ,["{{[[(())]]}}", true]
                        ]
    
    tests.map((test) => expect(Solution.isBalanced(test[0])).to.equal(test[1]))
  })

  it('test-1', () => {
    const tests = [["}][}}(}][))]", false]
                  ,["[](){()}", true]
                  ,["()", true]
                  ,["({}([][]))[]()", true]
                  ,["{)[](}]}]}))}(())(", false]
                  ,["([[)", false]
                  ]

    tests.map((test) => expect(Solution.isBalanced(test[0])).to.equal(test[1]))
  })
})