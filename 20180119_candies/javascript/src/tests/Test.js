const chai = require('chai')
const expect = chai.expect
const Solution = require('./../Solution')
const json_data = require('./../../../tests.json')
const test_cases = json_data.tests

describe('CandiesTests', () => {
  it('FromJSON', () => {
    const count = test_cases.length

    for (let i = 0; i < count; ++i) {
      const test = test_cases[i]
      let input = test.input.split(' ').map(Number)
      let output = test.output

      expect(Solution.candies(input)).to.equal(output)
    }
  })
})
