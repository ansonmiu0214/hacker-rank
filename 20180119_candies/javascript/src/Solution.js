process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentLine = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split('\n')
  main()
})

function readLine() {
  return input_stdin_array[input_currentLine++]
}

function candies(arr) {
  // Initialise result array
  const count = arr.length
  const candies = []
  candies.length = count
  candies.fill(1)

  // Populate "ups"
  for (let i = 1; i < count; ++i) {
    candies[i] = arr[i] > arr[i - 1] ? (candies[i - 1] + 1) : 1
  }

  // Populate "downs"
  for (let i = count - 2; i >= 0; --i) {
    if (arr[i] > arr[i + 1] && !(candies[i] > candies[i + 1])) {
      candies[i] = candies[i + 1] + 1
    }
  }

  return candies.reduce((x, y) => x + y)
}

function main() {
  const children_count = parseInt(readLine())
  const ratings = []
  for (let i = 0; i < children_count; ++i) {
    ratings.push(parseInt(readLine()))
  }
  const answer = candies(ratings)
  process.stdout.write(`${answer}\n`)
}

module.exports = {
  candies: candies
}