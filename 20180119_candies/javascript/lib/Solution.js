"use strict";

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentLine = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split('\n');
  main();
});

function readLine() {
  return input_stdin_array[input_currentLine++];
}

function candies(arr) {
  // Initialise result array
  var count = arr.length;
  var candies = [];
  candies.length = count;
  candies.fill(1);

  // Populate "ups"
  for (var i = 1; i < count; ++i) {
    candies[i] = arr[i] > arr[i - 1] ? candies[i - 1] + 1 : 1;
  }

  // Populate "downs"
  for (var _i = count - 2; _i >= 0; --_i) {
    if (arr[_i] > arr[_i + 1] && !(candies[_i] > candies[_i + 1])) {
      candies[_i] = candies[_i + 1] + 1;
    }
  }

  return candies.reduce(function (x, y) {
    return x + y;
  });
}

function main() {
  var children_count = parseInt(readLine());
  var ratings = [];
  for (var i = 0; i < children_count; ++i) {
    ratings.push(parseInt(readLine()));
  }
  var answer = candies(ratings);
  process.stdout.write(answer + "\n");
}

module.exports = {
  candies: candies
};