'use strict';

var chai = require('chai');
var expect = chai.expect;
var Solution = require('./../Solution');
var json_data = require('./../../../tests.json');
var test_cases = json_data.tests;

describe('CandiesTests', function () {
  it('FromJSON', function () {
    var count = test_cases.length;

    for (var i = 0; i < count; ++i) {
      var test = test_cases[i];
      var input = test.input.split(' ').map(Number);
      var output = test.output;

      expect(Solution.candies(input)).to.equal(output);
    }
  });
});