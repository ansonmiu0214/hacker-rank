#!/bin/python3

import sys

def candies(ratings):
  # Initialise results array
  count = len(ratings)
  candies = [1] * count

  # Record "ups"
  for i in range(1, count):
    if ratings[i] > ratings[i - 1]:
      candies[i] = candies[i - 1] + 1
    else:
      candies[i] = 1
  
  # Record "downs"
  for i in range(count - 2, -1, -1):
    if ratings[i] > ratings[i + 1] and not candies[i] > candies[i + 1]:
      candies[i] = candies[i + 1] + 1
  
  return sum(candies)

if __name__ == "__main__":
  num_of_candies = int(input().strip())
  ratings = [] 
  for i in range(num_of_candies):
    ratings.append(int(input().strip()))
  answer = candies(ratings)
  print(answer)