#!/bin/python3

import json, unittest, solution

TEST_PATH = "./../tests.json"

class CandiesTest(unittest.TestCase):

  def test_candies(self):
    with open(TEST_PATH) as json_file:
      test_data = json.load(json_file)["tests"]

      for case in test_data:
        arr = list(map(int, case["input"].strip().split(' ')))
        exp = int(case["output"])
        self.assertEqual(solution.candies(arr), exp)


if __name__ == "__main__":
  unittest.main()