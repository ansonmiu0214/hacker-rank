"use strict";

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentLine = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split('\n');
  main();
});

function readLine() {
  return input_stdin_array[input_currentLine++];
}

function power_sum(number, power) {
  var curr = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;

  var candidate = Math.pow(curr, power);

  // Base cases
  if (candidate == number) return 1; // match
  if (candidate > number) return 0; // overshot

  var persist = power_sum(number, power, curr + 1);
  var complement = power_sum(number - candidate, power, curr + 1);
  return persist + complement;
}

function main() {
  var number = parseInt(readLine());
  var power = parseInt(readLine());
  var answer = power_sum(number, power);
  process.stdout.write(answer + "\n");
}

module.exports = {
  power_sum: power_sum
};