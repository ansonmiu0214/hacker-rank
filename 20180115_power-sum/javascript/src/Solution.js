process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentLine = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split('\n')
  main()
})

function readLine() {
  return input_stdin_array[input_currentLine++]
}

function power_sum(number, power, curr = 1) {
  const candidate = Math.pow(curr, power)

  // Base cases
  if (candidate == number) return 1   // match
  if (candidate > number) return 0    // overshot

  const persist = power_sum(number, power, curr + 1)
  const complement = power_sum(number - candidate, power, curr + 1)
  return persist + complement
}

function main() {
  const number = parseInt(readLine())
  const power = parseInt(readLine())
  const answer = power_sum(number, power)
  process.stdout.write(`${answer}\n`)
}

module.exports = {
  power_sum: power_sum
}