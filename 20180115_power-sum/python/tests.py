#!/bin/python3

import unittest, solution, json

class PowerSumTest(unittest.TestCase):

  def test_powersum(self):
    with open("./../tests.json") as json_file:
      test_data = json.load(json_file)["tests"]
      
      for case in test_data:
        num, power = case["input"].strip().split(' ')
        num, power = [int(num), int(power)]
        exp = int(case["expected".strip()])
        self.assertEqual(solution.power_sum(num, power), exp)

if __name__ == "__main__":
  unittest.main()