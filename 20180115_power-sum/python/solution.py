#!/bin/python3

import sys

def power_sum(num, power, curr = 1):
  candidate = curr ** power

  if candidate == num:
    return 1  # match found

  if candidate > num:
    return 0  # overshot

  persist = power_sum(num, power, curr + 1)
  complement = power_sum(num - candidate, power, curr + 1)
  return persist + complement


if __name__ == "__main__":
  num = int(input().strip())
  power = int(input().strip())
  count_of_sums = power_sum(num, power)
  print(count_of_sums)