process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentline = 0

process.stdin.on('data', (data) => input_stdin += data)

process.stdin.on('end',  () => {
  input_stdin_array = input_stdin.split("\n");
  main()
})

function readLine() {
  return input_stdin_array[input_currentline++]
}

function main() {
  let temp = readLine().split(' ')
  const n = parseInt(temp[0])
  const m = parseInt(temp[1])
  let data = []
  data.length = n + 1
  data.fill(0)

  for (let i = 0; i < m; ++i) {
    temp = readLine().split(' ')
    const lo = parseInt(temp[0])
    const up = parseInt(temp[1])
    const inc = parseInt(temp[2])
    data[lo] += inc
    if (up + 1 <= n) data[up + 1] -= inc
  }

  let max = 0
  let accumulator = 0
  for (let i = 1; i <= n; ++i) {
    accumulator += data[i]
    max = Math.max(max, accumulator)
  }

  process.stdout.write(`${max}\n`)
}