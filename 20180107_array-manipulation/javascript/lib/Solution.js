"use strict";

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});

process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  main();
});

function readLine() {
  return input_stdin_array[input_currentline++];
}

function main() {
  var temp = readLine().split(' ');
  var n = parseInt(temp[0]);
  var m = parseInt(temp[1]);
  var data = [];
  data.length = n + 1;
  data.fill(0);

  for (var i = 0; i < m; ++i) {
    temp = readLine().split(' ');
    var lo = parseInt(temp[0]);
    var up = parseInt(temp[1]);
    var inc = parseInt(temp[2]);
    data[lo] += inc;
    if (up + 1 <= n) data[up + 1] -= inc;
  }

  var max = 0;
  var accumulator = 0;
  for (var _i = 1; _i <= n; ++_i) {
    accumulator += data[_i];
    max = Math.max(max, accumulator);
  }

  process.stdout.write(max + "\n");
}