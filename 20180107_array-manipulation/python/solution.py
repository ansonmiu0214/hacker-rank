#!/bin/python3

"""
  O(n) time, O(n) space solution, with tips from discussion forum.
  Only records changes in values (+100, -100, ...)
  One-pass iterate to find the values at the time, reduce max from this.
"""

import sys

if __name__ == "__main__":
  n, m = input().strip().split(' ')
  n, m = [int(n), int(m)]
  array = [0] * (n + 1)

  for _ in range(m):
    lo, up, inc = input().strip().split(' ')
    lo, up, inc = [int(lo), int(up), int(inc)]
    array[lo] += inc

    if up + 1 <= n:
      array[up + 1] -= inc  # negative change thereafter
  
  max_val = 0
  accumulator = 0
  for i in range(1, n + 1):
    accumulator += array[i]
    max_val = max(max_val, accumulator)
    
  print(max_val)