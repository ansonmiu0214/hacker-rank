#!/bin/python3

import sys

"""
Let m = len(a) and n = len(b)
O(mn) solution by generating a memoisation table, say f.
Assuming 1-indexing:
  * f[x][y] <==> b[1..y] can be obtained from a[1..x]
  * fill in table from x in [0,m] and y in [0,n]
  * x == 0 && x == y ==> True   (vacuously)
  * x == 0  ==> False           (b still has unmatched)
  * y == 0  ==> True            (only if a[1..x] is lowercase)
  * otherwise, recursive table lookup based on available options.
"""

def abbreviation(a, b):
  (size_a, size_b) = (len(a), len(b))

  # Initialise memoisation table
  table = [[False for col in range(size_b + 1)] for row in range(size_a + 1)]
  table[0][0] = True

  for row in range(1, size_a + 1):
    for col in range(size_b + 1):
      if col == 0:
        # Empty substring - check if remaining string is all lowercase
        a_islower = list(map(lambda c: c.islower(), a[:row]))
        table[row][col] = all(a_islower)

      else:
        # Parse characters
        A_char = a[row - 1]
        B_char = b[col - 1]

        if A_char == B_char:
          # Must consume B_char and recursively check
          table[row][col] = table[row - 1][col - 1]
        elif A_char.upper() == B_char:
          # Can either consume B_char or ignore the lowercase A_char
          table[row][col] = table[row - 1][col] or table[row - 1][col - 1]
        elif A_char.islower():
          # Ignore lowercase
          table[row][col] = table[row - 1][col]
        else:
          table[row][col] = False
  
  return table[size_a][size_b]

if __name__ == "__main__":
  num_of_tests = int(input().strip())
  for i in range(num_of_tests):
    a = list(input().strip())
    b = list(input().strip())
    answer = abbreviation(a, b)
    print("YES" if answer else "NO")
