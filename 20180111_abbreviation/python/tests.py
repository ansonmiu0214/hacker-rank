#!/bin/python3

import unittest, solution

class AbbreviationTest(unittest.TestCase):
  def test_case_0(self):
    a = list("daBcd")
    b = list("ABC")
    self.assertEqual(solution.abbreviation(a, b), True)
  
  def test_case_1(self):
    data = [(list("XXVVnDEFYgYeMXzWINQYHAQKKOZEYgSRCzLZAmUYGUGILjMDET"), list("XXVVDEFYYMXWINQYHAQKKOZEYSRCLZAUYGUGILMDETQVWU"), False)
           ,(list("PVJSNVBDXABZYYGIGFYDICWTFUEJMDXADhqcbzva"), list("PVJSNVBDXABZYYGIGFYDICWTFUEJMDXAD"), True)
           ,(list("QOTLYiFECLAGIEWRQMWPSMWIOQSEBEOAuhuvo"), list("QOTLYFECLAGIEWRQMWPSMWIOQSEBEOA"), True)
           ,(list("DRFNLZZVHLPZWIupjwdmqafmgkg"), list("DRFNLZZVHLPZWI"), True)
           ,(list("SLIHGCUOXOPQYUNEPSYVDaEZKNEYZJUHFXUIL"), list("SLIHCUOXOPQYNPSYVDEZKEZJUHFXUIHMGFP"), False)
           ]
    
    for (a, b, exp) in data:
      self.assertEqual(solution.abbreviation(a, b), exp)

if __name__ == "__main__":
  unittest.main()