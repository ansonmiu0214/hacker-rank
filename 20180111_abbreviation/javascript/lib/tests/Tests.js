'use strict';

var chai = require('chai');
var expect = chai.expect;
var Solution = require('./../Solution');

describe('Abbreviation-Tests', function () {
  it('small-input', function () {
    var a = Array.from("daBcd");
    var b = Array.from("ABC");
    expect(Solution.abbreviation(a, b)).to.equal(true);
  });
  it('long-input', function () {
    var a = Array.from("XXVVnDEFYgYeMXzWINQYHAQKKOZEYgSRCzLZAmUYGUGILjMDET");
    var b = Array.from("XXVVDEFYYMXWINQYHAQKKOZEYSRCLZAUYGUGILMDETQVWU");
    expect(Solution.abbreviation(a, b)).to.equal(false);
  });
});