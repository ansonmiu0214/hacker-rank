"use strict";

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentLine = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split('\n');
  main();
});

function readLine() {
  return input_stdin_array[input_currentLine++];
}

function abbreviation(a, b) {
  var a_size = a.length;
  var b_size = b.length;

  // Initialise memoisation table
  var table = [];
  for (var i = 0; i <= a_size; ++i) {
    var temp_col = [];
    temp_col.length = b_size + 1;
    temp_col.fill(false);
    table.push(temp_col);
  }
  table[0][0] = true;

  // Fill up table
  for (var row = 1; row <= a_size; ++row) {
    for (var col = 0; col <= b_size; ++col) {
      if (col == 0) {
        // Check whether remaining elements are lowercase
        var remains = a.slice(0, row);
        remains = remains.map(function (c) {
          return c.toLowerCase() == c;
        });
        table[row][col] = remains.reduce(function (x, y) {
          return x && y;
        });
      } else {
        // Parse characters
        var a_char = a[row - 1];
        var b_char = b[col - 1];

        if (a_char == b_char) {
          table[row][col] = table[row - 1][col - 1];
        } else if (a_char.toUpperCase() == b_char) {
          table[row][col] = table[row - 1][col - 1] || table[row - 1][col];
        } else if (a_char.toLowerCase() == a_char) {
          table[row][col] = table[row - 1][col];
        } else {
          table[row][col] = false;
        }
      }
    }
  }

  return table[a_size][b_size];
}

function main() {
  var num_of_tests = parseInt(readLine());
  for (var i = 0; i < num_of_tests; ++i) {
    var a = Array.from(readLine());
    var b = Array.from(readLine());
    var answer = abbreviation(a, b);
    process.stdout.write((answer ? "YES" : "NO") + "\n");
  }
}

// Export function for testing
module.exports = {
  abbreviation: abbreviation
};