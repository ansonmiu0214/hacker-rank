process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentLine = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split('\n')
  main()
})

function readLine() {
  return input_stdin_array[input_currentLine++]
}

function abbreviation(a, b) {
  const a_size = a.length
  const b_size = b.length

  // Initialise memoisation table
  let table = []
  for (let i = 0; i <= a_size; ++i) {
    let temp_col = []
    temp_col.length = b_size + 1
    temp_col.fill(false)
    table.push(temp_col)
  }
  table[0][0] = true

  // Fill up table
  for (let row = 1; row <= a_size; ++row) {
    for (let col = 0; col <= b_size; ++col) {
      if (col == 0) {
        // Check whether remaining elements are lowercase
        let remains = a.slice(0, row)
        remains = remains.map(c => c.toLowerCase() == c)
        table[row][col] = remains.reduce((x, y) => x && y)
      } else {
        // Parse characters
        const a_char = a[row - 1]
        const b_char = b[col - 1]

        if (a_char == b_char) {
          table[row][col] = table[row - 1][col - 1]
        } else if (a_char.toUpperCase() == b_char) {
          table[row][col] = table[row - 1][col - 1] || table[row - 1][col]
        } else if (a_char.toLowerCase() == a_char) {
          table[row][col] = table[row - 1][col]
        } else {
          table[row][col] = false
        }
      }
    }
  }

  return table[a_size][b_size]
}

function main() {
  const num_of_tests = parseInt(readLine())
  for (let i = 0; i < num_of_tests; ++i) {
    const a = Array.from(readLine())
    const b = Array.from(readLine())
    const answer = abbreviation(a, b)
    process.stdout.write(`${answer ? "YES" : "NO"}\n`)
  }
}

// Export function for testing
module.exports = {
  abbreviation: abbreviation
}