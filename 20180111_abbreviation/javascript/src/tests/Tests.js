const chai = require('chai')
const expect = chai.expect
const Solution = require('./../Solution')

describe('Abbreviation-Tests', () => {
  it('small-input', () => {
    const a = Array.from("daBcd")
    const b = Array.from("ABC")
    expect(Solution.abbreviation(a, b)).to.equal(true)
  })
  it('long-input', () => {
    const a = Array.from("XXVVnDEFYgYeMXzWINQYHAQKKOZEYgSRCzLZAmUYGUGILjMDET")
    const b = Array.from("XXVVDEFYYMXWINQYHAQKKOZEYSRCLZAUYGUGILMDETQVWU")
    expect(Solution.abbreviation(a, b)).to.equal(false)
  })
})