#!/bin/python3

import sys

MOD_CONSTANT = (10 ** 9) + 7

"""
O(n) time complexity solution

Rearrange the formula for summing_pieces to:
  arr[i] * contribution[i] for i in arr
  where contribution[i] = sum(len(piece) * freq(arr[i] in piece))

  e.g. for input [1,2,3], the possible combinations are:
  {[1],[2],[3]},  {[1,2],[3]}, {[1],[2,3]}, {[1,2,3]}
  so contributions[0] = (1 * 2) + (2 * 1) + (3 * 1)

A mathematical pattern can be identified w.r.t. contributions:
  given len(inputArray) = n,
    contributions[0] = (2 ^ n) - 1
    contributions[1] = contributions[0] + (2 ^ (n - 2)) - (2 ^ 0)
    ...
    contributions[k] = contributions[k - 1] + (2 ^ (n - 1 - k)) - (2 ^ (k - 1))
    ...
    contributions[n - 1] = (2 ^ n - 1)
"""

def summing_pieces(arr):
  n = len(arr)

  # Set up coefficient array
  coeffs = [0] * n

  # Initialise head 
  endVal = ((2 ** n ) - 1) % MOD_CONSTANT
  coeffs[0] = endVal
  answer = (endVal * arr[0]) % MOD_CONSTANT

  # DP power array
  powers = [pow(2, i, MOD_CONSTANT) for i in range(n - 1)]

  # Populate middle
  middle_constant = n - 2
  power = middle_constant
  mid = (n // 2) + 1
  for i in range(1, mid):
    curr = (coeffs[i - 1] + powers[power] - powers[middle_constant - power]) % MOD_CONSTANT
    answer = (answer + (curr * arr[i])) % MOD_CONSTANT
    coeffs[i] = curr
    power -= 1

  for i in range(mid, n - 1):
    answer = (answer + (coeffs[n - i - 1] * arr[i])) % MOD_CONSTANT
  
  # Initialise tail
  answer = (answer + (endVal * arr[-1])) % MOD_CONSTANT
  return answer

if __name__ == "__main__":
  n = int(input().strip())
  arr = list(map(int, input().strip().split(' ')))
  answer = summing_pieces(arr)
  print(answer)