"use strict";

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentLine = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split('\n');
  main();
});

function readLine() {
  return input_stdin_array[input_currentLine++];
}

var MOD_CONST = Math.pow(10, 9) + 7;

function summing_pieces(arr) {
  var len = arr.length;

  // Initialise power array
  var powers = [];

  for (var i = 0; i < len - 1; ++i) {
    powers.push(Math.pow(2, i) % MOD_CONST);
  }

  // Initialise coefficient array
  var coeffs = [];

  // Populate head value
  var endVal = (Math.pow(2, len) - 1) % MOD_CONST;
  coeffs.push(endVal);
  var answer = arr[0] * endVal % MOD_CONST;

  // Compute answer up to mid
  var mid = Math.floor(len / 2) + 1;
  var power = len - 2;
  var powerConst = power;
  for (var _i = 1; _i < mid; ++_i) {
    var curr = (coeffs[_i - 1] + powers[power] - powers[powerConst - power]) % MOD_CONST;
    answer = (answer + arr[_i] * curr) % MOD_CONST;
    --power;
    coeffs.push(curr);
  }

  // Repeat reverse
  for (var _i2 = mid; _i2 < len; ++_i2) {
    answer = (answer + arr[_i2] * coeffs[len - 1 - _i2]) % MOD_CONST;
  }

  return answer;
}

function main() {
  var n = parseInt(readLine());
  var arr = readLine().split(' ').map(Number);
  var answer = summing_pieces(arr);
  process.stdout.write(answer + "\n");
}