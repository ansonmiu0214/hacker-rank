process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin= ""
let input_stdin_array = ""
let input_currentLine = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split('\n')
  main()
})

function readLine() {
  return input_stdin_array[input_currentLine++]
}

const MOD_CONST = Math.pow(10, 9) + 7

function summing_pieces(arr) {
  const len = arr.length

  // Initialise power array
  const powers = []

  for (let i = 0; i < len - 1; ++i) {
    powers.push(Math.pow(2, i) % MOD_CONST)
  }

  // Initialise coefficient array
  const coeffs = []

  // Populate head value
  const endVal = (Math.pow(2, len) - 1) % MOD_CONST
  coeffs.push(endVal)
  let answer = (arr[0] * endVal) % MOD_CONST

  // Compute answer up to mid
  const mid = Math.floor(len / 2) + 1
  let power = len - 2
  const powerConst = power
  for (let i = 1; i < mid; ++i) {
    let curr = (coeffs[i - 1] + powers[power] - powers[powerConst - power]) % MOD_CONST
    answer = (answer + (arr[i] * curr)) % MOD_CONST
    --power
    coeffs.push(curr)
  }

  // Repeat reverse
  for (let i = mid; i < len ; ++i) {
    answer = (answer + (arr[i] * coeffs[len - 1 - i])) % MOD_CONST
  }
  
  return answer
}

function main() {
  const n = parseInt(readLine())
  const arr = readLine().split(' ').map(Number)
  const answer = summing_pieces(arr)
  process.stdout.write(`${answer}\n`)
}