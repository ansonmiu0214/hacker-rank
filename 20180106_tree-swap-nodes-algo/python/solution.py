#!/bin/python3

import sys, math
from collections import deque

def traverse_with_depth(tree, k, depth = 1):
  indices = [1]
  while indices:
    next_indices = []
    for index in indices:
      left, right = tree[index]
      if depth % k == 0:
        tree[index] = (right, left)
      
      if left > 0:
        next_indices.append(left)
      
      if right > 0:
        next_indices.append(right)
    
    indices = next_indices
    depth += 1

def inorder_traversal(tree, index = 1):
  items = []
  stack = deque()
  index = 1
  while True:
    while index > 0:                # go to leftmost empty node
      stack.append(index)
      index, _ = tree[index]

    if index < 0 and not stack:     # reached the rightmost empty node
      break
  
    index = stack.pop()             # no left leaf, get item and append
    items.append(index)
    _, index = tree[index]          # change pointer to right subtree and restart

  return items

if __name__ == "__main__":
  node_count = int(input())
  tree = [-1] * (node_count + 1)

  for i in range(node_count):
    left, right = input().strip().split(' ')
    children = (int(left), int(right))
    tree[i + 1] = children

  number_of_swaps = int(input())
  for i in range(number_of_swaps):
    depth = int(input())
    traverse_with_depth(tree, depth)
    state = inorder_traversal(tree)
    print(" ".join(str(val) for val in state))