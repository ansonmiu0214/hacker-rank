process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentline = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split('\n')
  main()
})

function readLine() {
  return input_stdin_array[input_currentline++]
}

function swap_nodes_at_depth(tree, k) {
  let curr = [1]
  let depth = 1

  while (curr.length > 0) {
    let next = []
    let should_swap = depth % k == 0
    curr.map(index => {
      let [left, right] = tree[index]
      
      if (should_swap) tree[index] = [right, left]
      if (left > 0) next.push(left)
      if (right > 0) next.push(right)
    })
    ++depth
    curr = next
  }
}

function inorder_traversal(tree) {
  let nodes = []
  let curr = 1
  let stack = []

  while (true) {
    if (curr < 0 && stack.length == 0) break

    while (curr > 0) {
      stack.push(curr)
      curr = tree[curr][0] 
    }

    curr = stack.pop()
    nodes.push(curr)
    curr = tree[curr][1]
  }

  return nodes.join(" ")
}

function main() {
  const node_count = parseInt(readLine())
  let tree = [[]]

  for (let i = 0; i < node_count; ++i) {
    const children = readLine().split(' ').map(Number)
    tree.push(children)
  }

  const swap_count = parseInt(readLine())
  for (let i = 0; i < swap_count; ++i) {
    const swap_k = parseInt(readLine())
    swap_nodes_at_depth(tree, swap_k)
    process.stdout.write(`${inorder_traversal(tree)}\n`)
  }
}