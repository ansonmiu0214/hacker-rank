"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split('\n');
  main();
});

function readLine() {
  return input_stdin_array[input_currentline++];
}

function swap_nodes_at_depth(tree, k) {
  var curr = [1];
  var depth = 1;

  var _loop = function _loop() {
    var next = [];
    var should_swap = depth % k == 0;
    curr.map(function (index) {
      var _tree$index = _slicedToArray(tree[index], 2),
          left = _tree$index[0],
          right = _tree$index[1];

      if (should_swap) tree[index] = [right, left];
      if (left > 0) next.push(left);
      if (right > 0) next.push(right);
    });
    ++depth;
    curr = next;
  };

  while (curr.length > 0) {
    _loop();
  }
}

function inorder_traversal(tree) {
  var nodes = [];
  var curr = 1;
  var stack = [];

  while (true) {
    if (curr < 0 && stack.length == 0) break;

    while (curr > 0) {
      stack.push(curr);
      curr = tree[curr][0];
    }

    curr = stack.pop();
    nodes.push(curr);
    curr = tree[curr][1];
  }

  return nodes.join(" ");
}

function main() {
  var node_count = parseInt(readLine());
  var tree = [[]];

  for (var i = 0; i < node_count; ++i) {
    var children = readLine().split(' ').map(Number);
    tree.push(children);
  }

  var swap_count = parseInt(readLine());
  for (var _i = 0; _i < swap_count; ++_i) {
    var swap_k = parseInt(readLine());
    swap_nodes_at_depth(tree, swap_k);
    process.stdout.write(inorder_traversal(tree) + "\n");
  }
}