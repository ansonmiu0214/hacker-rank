"use strict";

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentLine = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split('\n');
  main();
});

function readLine() {
  return input_stdin_array[input_currentLine++];
}

function cost(arr) {
  var arr_len = arr.length;

  // Initialise dynamic programming array
  var table = [];
  table.length = arr_len;
  table.fill([0, 0]);

  for (var i = 1; i < arr_len; ++i) {
    // Parse values
    var prev_low = table[i - 1][0];
    var prev_high = table[i - 1][1];

    var new_low = Math.max(prev_low, Math.abs(1 - arr[i - 1]) + prev_high);
    var new_high = Math.max(Math.abs(arr[i] - 1) + prev_low, Math.abs(arr[i] - arr[i - 1]) + prev_high);

    table[i][0] = new_low;
    table[i][1] = new_high;
  }

  return Math.max(table[arr_len - 1][0], table[arr_len - 1][1]);
}

function main() {
  var num_of_tests = parseInt(readLine());
  for (var i = 0; i < num_of_tests; ++i) {
    var arr_len = parseInt(readLine());
    var arr = readLine().split(' ');
    process.stdout.write(cost(arr) + "\n");
  }
}

module.exports = {
  cost: cost
};