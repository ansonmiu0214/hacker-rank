'use strict';

var chai = require('chai');
var expect = chai.expect;
var Solution = require('./../Solution');

describe('Sherlock-Test', function () {
  it('test-0', function () {
    var arr = [10, 1, 10, 1, 10];
    var exp = 36;
    expect(Solution.cost(arr)).to.equal(exp);
  });

  it('test-1', function () {
    var arr = [2, 4, 3];
    var exp = 6;
    expect(Solution.cost(arr)).to.equal(exp);
  });
});