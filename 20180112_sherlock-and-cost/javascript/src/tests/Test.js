const chai = require('chai')
const expect = chai.expect
const Solution = require('./../Solution')

describe('Sherlock-Test', () => {
  it('test-0', () => {
    const arr = [10, 1, 10, 1, 10]
    const exp = 36
    expect(Solution.cost(arr)).to.equal(exp)
  })

  it('test-1', () => {
    const arr = [2, 4, 3]
    const exp = 6
    expect(Solution.cost(arr)).to.equal(exp)
  })
})