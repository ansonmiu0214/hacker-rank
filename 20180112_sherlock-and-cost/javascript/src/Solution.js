process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentLine = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split('\n')
  main()
})

function readLine() {
  return input_stdin_array[input_currentLine++]
}

function cost(arr) {
  const arr_len = arr.length

  // Initialise dynamic programming array
  const table = []
  table.length = arr_len
  table.fill([0, 0])

  for (let i = 1; i < arr_len; ++i) {
    // Parse values
    const prev_low = table[i - 1][0]
    const prev_high = table[i - 1][1]

    const new_low = Math.max(prev_low, Math.abs(1 - arr[i - 1]) + prev_high)
    const new_high = Math.max(Math.abs(arr[i] - 1) + prev_low, Math.abs(arr[i] - arr[i - 1]) + prev_high)
    
    table[i][0] = new_low
    table[i][1] = new_high
  }

  return Math.max(table[arr_len - 1][0], table[arr_len - 1][1])
}

function main() {
  const num_of_tests = parseInt(readLine())
  for (let i = 0; i < num_of_tests; ++i) {
    const arr_len = parseInt(readLine())
    let arr = readLine().split(' ')
    process.stdout.write(`${cost(arr)}\n`)
  }
}

module.exports = {
  cost: cost
}