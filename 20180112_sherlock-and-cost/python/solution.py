#!/bin/python3

"""
Let input array of length n
Solution has time-complexity O(n) and space-complexity O(n)

Using the dynamic programming approach, initialise array as table[n][2] where
  * table[i][0] := max cost of arr[0..i] using 1
  * table[i][1] := max cost of arr[0..i] using arr[i]

Apply the following rules:
  * table[0][_] := max cost = 0 because there is only one element
  * table[i][0] := curr_elem is 1 AND find max between:
      * use table[i-1][0] with prev_elem 1        , so max cost = table[i-1][0] + abs(1 - 1)
      * use table[i-1][1] with prev_elem arr[i-1] , so max cost = table[i-1][1] + abs(1 - arr[i-1])
  * table[i][1] := curr_elem is arr[i] AND find max between:
      * use table[i-1][0] with prev_elem 1        , so max cost = table[i-1][0] + abs(arr[i] - 1)
      * use table[i-1][1] with prev_elem arr[i-1] , so max cost = table[i-1][1] + abs(arr[i] - arr[i])

Answer found by max(table[n-1][0], table[n-1][1])
"""

import sys

def cost(arr):
  arr_len = len(arr)

  # Initialise memoisation table
  table = [[0] * 2 for row in range(arr_len)]

  # Fill in table
  for row in range(1, arr_len):
    prev_low = table[row - 1][0]
    prev_high = table[row - 1][1]

    # Column 0: max cost taking A[i] = 1
    table[row][0] = max(prev_low, abs(1 - arr[row - 1]) + prev_high)

    # Column 1: max cost taking A[i] = arr[i]
    table[row][1] = max(abs(arr[row] - 1) + prev_low, abs(arr[row] - arr[row - 1]) + prev_high)
  
  return max(table[arr_len - 1][0], table[arr_len - 1][1])

if __name__ == "__main__":
  num_of_tests = int(input().strip())
  for i in range(num_of_tests):
    arr_len = int(input().strip())
    arr = list(map(int, input().strip().split(' ')))
    result = cost(arr)
    print(result)