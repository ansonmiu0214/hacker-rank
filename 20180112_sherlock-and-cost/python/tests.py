#!/bin/python3

import unittest, solution

class CostTest(unittest.TestCase):

  def test_0(self):
    arr = [10, 1, 10, 1, 10]
    exp = 36
    self.assertEqual(solution.cost(arr), exp)

  def test_1(self):
    arr = [2,4,3]
    exp = 6
    self.assertEqual(solution.cost(arr), exp)


if __name__ == "__main__":
  unittest.main()