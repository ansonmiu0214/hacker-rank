const chai = require('chai')
const expect = chai.expect
const Solution = require('./../Solution')

describe('EqualTest', () => {
  it('test-0', () => {
    const testcase = [2,2,3,7]
    const exp = 2
    expect(Solution.equal(testcase)).to.equal(exp)
  })

  it('test-1', () => {
    const testcase = [1,5,5]
    const exp = 3
    expect(Solution.equal(testcase)).to.equal(exp)
  })
})