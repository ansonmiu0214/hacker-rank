process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentLine = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split('\n')
  main()
})

function readLine() {
  return input_stdin_array[input_currentLine++]
}

function equal(array) {
  // Find minimum value of array with linear search in O(n) time
  const minVal = array.reduce((x, y) => Math.min(x, y))

  // Find minimum operation count in set { minVal, minVal - 1, ... , minVal - 4}
  const possibleMinOps = []
  for (let i = 0; i < 5; ++i) {
    const localMin = minVal - i
    const requiredOps = array.map((val) => getMinimumOperations(val - localMin))
    possibleMinOps.push(requiredOps.reduce((x, y) => x + y))
  }

  return possibleMinOps.reduce((x, y) => Math.min(x, y))
}

function getMinimumOperations(diff) {
  const quot_5 = Math.trunc(diff / 5)
  const rem_5 = diff % 5

  return quot_5 + Math.trunc(rem_5 / 2) + (rem_5 % 2)
}

function main() {
  const num_of_tests = parseInt(readLine())

  for (let i = 0; i < num_of_tests; ++i) {
    const num_of_elems = parseInt(readLine())
    let list = readLine().split(' ')
    list = list.map(Number)
    const answer = equal(list)
    process.stdout.write(`${answer}\n`)
  }
}

// Export function
module.exports = {
  equal: equal
}