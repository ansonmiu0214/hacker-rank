"use strict";

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentLine = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split('\n');
  main();
});

function readLine() {
  return input_stdin_array[input_currentLine++];
}

function equal(array) {
  // Find minimum value of array with linear search in O(n) time
  var minVal = array.reduce(function (x, y) {
    return Math.min(x, y);
  });

  // Find minimum operation count in set { minVal, minVal - 1, ... , minVal - 4}
  var possibleMinOps = [];

  var _loop = function _loop(i) {
    var localMin = minVal - i;
    var requiredOps = array.map(function (val) {
      return getMinimumOperations(val - localMin);
    });
    possibleMinOps.push(requiredOps.reduce(function (x, y) {
      return x + y;
    }));
  };

  for (var i = 0; i < 5; ++i) {
    _loop(i);
  }

  return possibleMinOps.reduce(function (x, y) {
    return Math.min(x, y);
  });
}

function getMinimumOperations(diff) {
  var quot_5 = Math.trunc(diff / 5);
  var rem_5 = diff % 5;

  return quot_5 + Math.trunc(rem_5 / 2) + rem_5 % 2;
}

function main() {
  var num_of_tests = parseInt(readLine());

  for (var i = 0; i < num_of_tests; ++i) {
    var num_of_elems = parseInt(readLine());
    var list = readLine().split(' ');
    list = list.map(Number);
    var answer = equal(list);
    process.stdout.write(answer + "\n");
  }
}

// Export function
module.exports = {
  equal: equal
};