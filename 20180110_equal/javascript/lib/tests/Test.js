'use strict';

var chai = require('chai');
var expect = chai.expect;
var Solution = require('./../Solution');

describe('EqualTest', function () {
  it('test-0', function () {
    var testcase = [2, 2, 3, 7];
    var exp = 2;
    expect(Solution.equal(testcase)).to.equal(exp);
  });

  it('test-1', function () {
    var testcase = [1, 5, 5];
    var exp = 3;
    expect(Solution.equal(testcase)).to.equal(exp);
  });
});