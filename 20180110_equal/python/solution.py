#!/bin/python3

"""
O(nlog(n)) solution
Increasing every other element by n has the equivalent effect of
decreasing the existing element by n.

* Find the minimum of the array, say x
* For each in {x, x-1, x-2, x-3, x-4}:
  * find minimum operations required, store in minArray
* Reduce the minArray to find minimum operations
"""

def equal(arr):
  arr.sort()
  count = len(arr)
  origMin = arr[0]

  opCounts = []
  for i in range(5):
    minVal = origMin - i
    minOpCount = 0
    for j in range(count):
      diff = arr[j] - minVal
      minOpCount += getOpCount(diff)
    opCounts.append(minOpCount)

  return min(opCounts)

def getOpCount(diff):
  (quot_5, rem_5) = divmod(diff, 5)
  (quot_2, rem_2) = divmod(rem_5, 2)
  return quot_5 + quot_2 + rem_2
      

if __name__ == "__main__":
  num_of_tests = int(input().strip())
  for t in range(num_of_tests):
    num_of_ppl = int(input().strip())
    init_count = list(map(int, input().strip().split(' ')))
    result = equal(init_count)
    print(result)
