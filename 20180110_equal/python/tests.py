#!/bin/python3

import unittest, solution

class EqualTest(unittest.TestCase):

  def test_case_0(self):
    data = [2,2,3,7]
    exp = 2
    self.assertEqual(solution.equal(data), exp)
  
  def test_case_1(self):
    data = [1,5,5]
    exp = 3
    self.assertEqual(solution.equal(data), exp)

if __name__ == "__main__":
  unittest.main()