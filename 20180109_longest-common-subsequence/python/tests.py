#!/bin/python3

import unittest, solution

class LCSTests(unittest.TestCase):
  def test_case_0(self):
    a = [1,2,3,4,1]
    b = [3,4,1,2,1,3]
    exp = [1,2,3]
    self.assertEqual(solution.longestCommonSubsequence(a, b), exp)

if __name__ == "__main__":
  unittest.main()
