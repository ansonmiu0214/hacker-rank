#!/bin/python3

import sys

"""
Implementation of solution explained by: https://youtu.be/V5hZoJ6uK-s
O(nm) solution via dynamic programming approach

Main problem: given lists {a, b} where n = len(a) and m = len(b), print LCS(a, b)
              <==> print LCS(a[1, n], b[1, m])

Subproblems: find the `length' of LCS(a[1, i], b[1, j]) for i in [0,n] and j in [0,m]
             let this algorithm be defined as f(i, j)

Since the value of f(i, j) depends on whether a[i] == b[j] we can backtrack over the
memoisation table (built in O(nm) time complexity) to reconstruct the subsequence. 
"""

def longestCommonSubsequence(a, b):
  # Initialise memoisation table
  a_len = len(a)
  b_len = len(b)
  lcs_lengths = [[0 for j in range(b_len + 1)] for i in range(a_len + 1)]

  for row in range(1, a_len + 1):
    for col in range(1, b_len + 1):
      if a[row - 1] == b[col - 1]:    # adjust for Python 0-index lists
        lcs_lengths[row][col] = lcs_lengths[row - 1][col - 1] + 1
      else:
        lcs_lengths[row][col] = max(lcs_lengths[row - 1][col], lcs_lengths[row][col - 1])

  return buildSubsequence(lcs_lengths, a_len, b_len, a, b)

def buildSubsequence(table, row, col, a, b):
  if row == 0 or col == 0:
    return []

  if a[row - 1] == b[col - 1]:
    subseq = buildSubsequence(table, row - 1, col - 1, a, b)
    subseq.append(a[row - 1])
    return subseq
  elif table[row][col] == table[row - 1][col]:
    return buildSubsequence(table, row - 1, col, a, b)
  else:
    return buildSubsequence(table, row, col - 1, a, b)


if __name__ == "__main__":
  n, m = input().strip().split(' ')
  n, m = [int(n), int(m)]
  a = list(map(int, input().strip().split(' ')))
  b = list(map(int, input().strip().split(' ')))
  lcs = longestCommonSubsequence(a, b)
  print(" ".join(map(str, lcs)))