const chai = require('chai')
const expect = chai.expect
const Solution = require('./../Solution')

describe('longest-common-subsequence', () => {
  it('test-0', () => {
    const a = [1,2,3,4,1]
    const b = [3,4,1,2,1,3]
    const exp = [1,2,3]

    expect(Solution.longestCommonSubsequence(a, b)).to.deep.equal(exp)
  })
})