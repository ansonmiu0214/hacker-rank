process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentLine = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split("\n")
  main()
})

function readLine() {
  return input_stdin_array[input_currentLine++]
}

function longestCommonSubsequence(a, b) {
  const a_len = a.length
  const b_len = b.length

  // Initialise memoization table
  const lcs_lengths = []
  for (let i = 0; i <= a_len; ++i) {
    let temp = []
    temp.length = b_len + 1
    temp.fill(0)
    lcs_lengths.push(temp)
  }

  // Fill up memoization table
  for (let i = 1; i <= a_len; ++i) {
    for (let j = 1; j <= b_len; ++j) {
      if (a[i - 1] === b[j - 1]) {
        lcs_lengths[i][j] = lcs_lengths[i - 1][j - 1] + 1
      } else {
        lcs_lengths[i][j] = Math.max(lcs_lengths[i - 1][j], lcs_lengths[i][j - 1])
      }
    }
  }
  
  return rebuildLCS(lcs_lengths, a_len, b_len, a, b)
}

function rebuildLCS(table, row, col, a, b) {
  if (row === 0 || col === 0) {
    return []   // base case
  }

  if (a[row - 1] === b[col - 1]) {
    const seq = rebuildLCS(table, row - 1, col - 1, a, b)
    seq.push(a[row - 1])
    return seq
  }

  if (table[row][col] === table[row - 1][col]) {
    return rebuildLCS(table, row - 1, col, a, b)
  } else {
    return rebuildLCS(table, row, col - 1, a, b)
  }
}

function main() {
  let temp = readLine().split(' ')
  const n = parseInt(temp[0])
  const m = parseInt(temp[1])
  const a = readLine().split(' ')
  const b = readLine().split(' ')

  const lcs = longestCommonSubsequence(a, b)
  process.stdout.write(`${lcs.join(' ')}\n`)
}

// Export
module.exports = { longestCommonSubsequence: longestCommonSubsequence }