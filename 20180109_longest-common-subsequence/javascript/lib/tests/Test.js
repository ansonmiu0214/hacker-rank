'use strict';

var chai = require('chai');
var expect = chai.expect;
var Solution = require('./../Solution');

describe('longest-common-subsequence', function () {
  it('test-0', function () {
    var a = [1, 2, 3, 4, 1];
    var b = [3, 4, 1, 2, 1, 3];
    var exp = [1, 2, 3];

    expect(Solution.longestCommonSubsequence(a, b)).to.deep.equal(exp);
  });
});