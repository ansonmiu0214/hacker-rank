"use strict";

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentLine = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  main();
});

function readLine() {
  return input_stdin_array[input_currentLine++];
}

function longestCommonSubsequence(a, b) {
  var a_len = a.length;
  var b_len = b.length;

  // Initialise memoization table
  var lcs_lengths = [];
  for (var i = 0; i <= a_len; ++i) {
    var temp = [];
    temp.length = b_len + 1;
    temp.fill(0);
    lcs_lengths.push(temp);
  }

  // Fill up memoization table
  for (var _i = 1; _i <= a_len; ++_i) {
    for (var j = 1; j <= b_len; ++j) {
      if (a[_i - 1] === b[j - 1]) {
        lcs_lengths[_i][j] = lcs_lengths[_i - 1][j - 1] + 1;
      } else {
        lcs_lengths[_i][j] = Math.max(lcs_lengths[_i - 1][j], lcs_lengths[_i][j - 1]);
      }
    }
  }

  return rebuildLCS(lcs_lengths, a_len, b_len, a, b);
}

function rebuildLCS(table, row, col, a, b) {
  if (row === 0 || col === 0) {
    return []; // base case
  }

  if (a[row - 1] === b[col - 1]) {
    var seq = rebuildLCS(table, row - 1, col - 1, a, b);
    seq.push(a[row - 1]);
    return seq;
  }

  if (table[row][col] === table[row - 1][col]) {
    return rebuildLCS(table, row - 1, col, a, b);
  } else {
    return rebuildLCS(table, row, col - 1, a, b);
  }
}

function main() {
  var temp = readLine().split(' ');
  var n = parseInt(temp[0]);
  var m = parseInt(temp[1]);
  var a = readLine().split(' ');
  var b = readLine().split(' ');

  var lcs = longestCommonSubsequence(a, b);
  process.stdout.write(lcs.join(' ') + "\n");
}

// Export
module.exports = { longestCommonSubsequence: longestCommonSubsequence };