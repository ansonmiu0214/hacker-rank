#!/bin/python3

import sys

def maxVals(arr):
  size = len(arr)
  max_elem = arr[0]
  sum_subseq = max(arr[0], 0)
  local_max = arr[0]
  global_max = arr[0]

  for i in range(1, size):
    elem = arr[i]
    max_elem = max(max_elem, elem)
    if elem > 0:
      sum_subseq += elem

    local_max = max(elem, local_max + elem)
    global_max = max(global_max, local_max)
  
  if max_elem < 0:
    sum_subseq = max_elem

  return (global_max, sum_subseq)


if __name__ == "__main__":
  num_of_tests = int(input().strip())
  for t in range(num_of_tests):
    num_of_elems = int(input().strip())
    arr = list(map(int, input().strip().split(' ')))
    (sum_subarray, sum_subseq) = maxVals(arr)
    print("{} {}".format(sum_subarray, sum_subseq))