process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentLine = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split('\n')
  main()
})

function readLine() {
  return input_stdin_array[input_currentLine++]
}

function max_vals(arr) {
  arr = arr.map(Number)
  const size = arr.length
  
  let max_elem = arr[0]
  let sum_subseq = Math.max(0, arr[0])
  let local_max = arr[0]
  let global_max = arr[0]

  for (let i = 1; i < size; ++i) {
    const elem = arr[i]

    max_elem = Math.max(max_elem, elem)
    if (elem > 0) sum_subseq += elem

    local_max = Math.max(elem, local_max + elem)
    global_max = Math.max(global_max, local_max)
  }

  if (max_elem < 0) sum_subseq = max_elem
  process.stdout.write(`${global_max} ${sum_subseq}\n`)
}

function main() {
  const num_of_tests = parseInt(readLine())
  for (let t = 0; t < num_of_tests; ++t) {
    const arr_len = parseInt(readLine())
    const arr = readLine().split(' ')
    max_vals(arr)
  }
}