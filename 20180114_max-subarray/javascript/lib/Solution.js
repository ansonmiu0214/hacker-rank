"use strict";

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentLine = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split('\n');
  main();
});

function readLine() {
  return input_stdin_array[input_currentLine++];
}

function max_vals(arr) {
  arr = arr.map(Number);
  var size = arr.length;

  var max_elem = arr[0];
  var sum_subseq = Math.max(0, arr[0]);
  var local_max = arr[0];
  var global_max = arr[0];

  for (var i = 1; i < size; ++i) {
    var elem = arr[i];

    max_elem = Math.max(max_elem, elem);
    if (elem > 0) sum_subseq += elem;

    local_max = Math.max(elem, local_max + elem);
    global_max = Math.max(global_max, local_max);
  }

  if (max_elem < 0) sum_subseq = max_elem;
  process.stdout.write(global_max + " " + sum_subseq + "\n");
}

function main() {
  var num_of_tests = parseInt(readLine());
  for (var t = 0; t < num_of_tests; ++t) {
    var arr_len = parseInt(readLine());
    var arr = readLine().split(' ');
    max_vals(arr);
  }
}