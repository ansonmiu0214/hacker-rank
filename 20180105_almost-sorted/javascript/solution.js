process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
  input_stdin += data;
});

process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  main();
});

function readLine() {
  return input_stdin_array[input_currentline++];
}

/////////////// ignore above this line ////////////////////

function almostSorted(arr) {
  // Complete this function
  arr_sorted = arr.slice();
  arr_sorted.sort(function(x, y) {
    return x - y;
  });

  conflicts = [];

  for (var i in arr) {
    if (arr[i] != arr_sorted[i]) conflicts.push(i);
  }

  conflicts_count = conflicts.length;

  if (conflicts_count == 0) return "yes";

  first_index = Number(conflicts[0]);
  last_index = Number(conflicts[conflicts_count - 1]);

  if (conflicts_count == 2) return "yes\n" + "swap " + (first_index + 1) + " " + (last_index + 1); 

  // Attempt reversal
  var subrange_rev = arr.slice(first_index, last_index + 1);
  var subrange_sorted = arr.slice(first_index, last_index + 1);
  subrange_rev.reverse();
  subrange_sorted.sort(function(x, y) {
    return x - y;
  });

  if (JSON.stringify(subrange_rev) != JSON.stringify(subrange_sorted)) {
    return "no";
  } else {
    ++first_index;
    ++last_index;
    return "yes\n" + "reverse " + first_index + " " + last_index;
  } 
}

function main() {
  var n = parseInt(readLine());
  arr = readLine().split(' ');
  arr = arr.map(Number);
  var result = almostSorted(arr);
  process.stdout.write("" + result + "\n");
}

// Export methods for unit testing
var methods = {
  almostSorted: almostSorted
};
module.exports = methods;