var chai = require('chai');   // assertion library
var expect = chai.expect;
var solution = require('./../solution.js');

describe('Solution', function() {
  it('sample test case 0', function() {
    var arr = [4,2];
    var exp = "yes\nswap 1 2";
    expect(solution.almostSorted(arr)).to.equal(exp);
  });

  it('sample test case 1', function() {
    var arr = [3,1,2];
    var exp = "no";
    expect(solution.almostSorted(arr)).to.equal(exp);
  });

  it('sample test case 2', function() {
    var arr = [1,5,4,3,2,6];
    var exp = "yes\nreverse 2 5";
    expect(solution.almostSorted(arr)).to.equal(exp);
  });

  it('sample test case 3', function() {
    var arr = [1,2,3,4,5,6];
    var exp = "yes";
    expect(solution.almostSorted(arr)).to.equal(exp);
  });
});