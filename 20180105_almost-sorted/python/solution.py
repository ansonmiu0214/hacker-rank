#!/bin/python3

import sys

def almostSorted(arr):
  num_count = len(arr)
  arr_sorted = sorted(arr)

  conflicts = [ i for i in range(num_count) if arr[i] != arr_sorted[i] ]
  conflict_count = len(conflicts)

  if conflict_count == 0:
    return "yes"    # list already sorted
  
  if conflict_count == 2:
    index_1, index_2 = conflicts
    return "yes\nswap {} {}".format(index_1 + 1, index_2 + 1) 
  
  # Attempt reversal
  last_index = conflicts[-1]
  first_index = conflicts[0]
  subrange = arr[first_index:(last_index + 1)]

  # Check for continuous range
  if max(subrange) != subrange[0] or min(subrange) != subrange[-1]:
    return "no"

  rev_subrange = list(reversed(subrange))
  if rev_subrange == sorted(subrange):
    return "yes\nreverse {} {}".format(first_index + 1, last_index + 1)
  else:
    return "no"

if __name__ == "__main__":
  n = int(input().strip())
  arr = list(map(int, input().strip().split(' ')))
  answer = almostSorted(arr)
  print(answer)
