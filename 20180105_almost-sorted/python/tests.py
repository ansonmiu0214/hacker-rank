import unittest, solution

testcases = [([4,2], "yes\nswap 1 2")
            ,([3,1,2], "no")
            ,([1,5,4,3,2,6], "yes\nreverse 2 5")
            ]

class TestSolution(unittest.TestCase):

  def test_samplecases(self):
    for (arr, exp) in testcases:
      self.assertEqual(solution.almostSorted(arr), exp)

if __name__ == '__main__':
  unittest.main()