const chai = require('chai')
const expect = chai.expect
const Solution = require('./../Solution')

describe('SubstringTest', () => {
  it('short-input', () => {
    const num = 123
    const exp = 164
    expect(Solution.substrings(num)).to.equal(exp)
  })

})