process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentLine = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split('\n')
  main()
})

function readLine() {
  return input_stdin_array[input_currentLine++]
}

function substrings(num) {
  const MOD_CONST = Math.pow(10, 9) + 7
  const arr = num.toString(10).split('').map(Number)
  const size = arr.length

  // Initialise DP array
  const table = []
  table.length = size
  table.fill(0)

  table[0] = arr[0]
  let sum = table[0]

  for (let i = 1; i < size; ++i) {
    table[i] = ((table[i - 1] * 10) + (arr[i] * (i + 1))) % MOD_CONST
    sum = (sum + table[i]) % MOD_CONST
  }

  return sum
}

function main() {
  const num = readLine()
  const answer = substrings(num)
  process.stdout.write(`${answer}\n`)
}

module.exports = {
  substrings: substrings
}