"use strict";

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentLine = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split('\n');
  main();
});

function readLine() {
  return input_stdin_array[input_currentLine++];
}

function substrings(num) {
  var MOD_CONST = Math.pow(10, 9) + 7;
  var arr = num.toString(10).split('').map(Number);
  var size = arr.length;

  // Initialise DP array
  var table = [];
  table.length = size;
  table.fill(0);

  table[0] = arr[0];
  var sum = table[0];

  for (var i = 1; i < size; ++i) {
    table[i] = (table[i - 1] * 10 + arr[i] * (i + 1)) % MOD_CONST;
    sum = (sum + table[i]) % MOD_CONST;
  }

  return sum;
}

function main() {
  var num = readLine();
  var answer = substrings(num);
  process.stdout.write(answer + "\n");
}

module.exports = {
  substrings: substrings
};