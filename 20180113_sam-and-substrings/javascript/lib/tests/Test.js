'use strict';

var chai = require('chai');
var expect = chai.expect;
var Solution = require('./../Solution');

describe('SubstringTest', function () {
  it('short-input', function () {
    var num = 123;
    var exp = 164;
    expect(Solution.substrings(num)).to.equal(exp);
  });
});