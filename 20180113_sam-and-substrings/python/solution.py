#!/bin/python3

import sys

"""
Assume input array is of length n.
This solution is O(n) time-complexity, O(n) space-complexity.

Identify recurrence relation.
Define table[k] := sum of substrings ending with arr[k]
table[0] = arr[0]
table[k] = (table[k - 1] * 10) + (arr[k] * (k + 1))
"""

MOD_CONSTANT = (10 ** 9) + 7

def substrings(arr):
  arr_len = len(arr)

  # Initialise DP array
  substring_sums = [0] * arr_len

  substring_sums[0] = arr[0]
  result = substring_sums[0]
  for k in range(1, arr_len):
    substring_sums[k] = (substring_sums[k - 1] * 10 + (arr[k] * (k + 1))) % MOD_CONSTANT
    result = (result + substring_sums[k]) % MOD_CONSTANT

  return result

if __name__ == "__main__":
  num = list(map(int, list(str(input().strip()))))
  count = substrings(num)
  print(count)
  