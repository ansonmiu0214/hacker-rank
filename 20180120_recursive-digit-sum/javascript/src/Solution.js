process.stdin.resume()
process.stdin.setEncoding('ascii')

let input_stdin = ""
let input_stdin_array = ""
let input_currentLine = 0

process.stdin.on('data', (data) => input_stdin += data)
process.stdin.on('end', () => {
  input_stdin_array = input_stdin.split('\n')
  main()
})

function readLine() {
  return input_stdin_array[input_currentLine++]
}

function digitSum(number, freq = 1) {
  let digits = []
  while (number >= 10 || freq > 1) {
    digits = Array.from(String(number)).map(Number)
    number = digits.map((num) => num * freq).reduce((x, y) => x + y)
    freq = 1
  } 

  // Base case
  return number
}

function main() {
  const [num, freq] = readLine().split(' ').map(Number)
  const answer = digitSum(num, freq)
  process.stdout.write(`${answer}\n`)
}