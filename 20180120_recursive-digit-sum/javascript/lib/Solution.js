"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentLine = 0;

process.stdin.on('data', function (data) {
  return input_stdin += data;
});
process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split('\n');
  main();
});

function readLine() {
  return input_stdin_array[input_currentLine++];
}

function digitSum(number) {
  var freq = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

  var digits = [];
  while (number >= 10 || freq > 1) {
    digits = Array.from(String(number)).map(Number);
    number = digits.map(function (num) {
      return num * freq;
    }).reduce(function (x, y) {
      return x + y;
    });
    freq = 1;
  }

  // Base case
  return number;
}

function main() {
  var _readLine$split$map = readLine().split(' ').map(Number),
      _readLine$split$map2 = _slicedToArray(_readLine$split$map, 2),
      num = _readLine$split$map2[0],
      freq = _readLine$split$map2[1];

  var answer = digitSum(num, freq);
  process.stdout.write(answer + "\n");
}