#!/bin/python3

import sys

def digitSum(num, freq = 1):
  # Base case
  if num < 10 and freq == 1:
    return num

  # Recursive case
  digits = list(map(int, list(str(num))))

  # Reduce recursive digit sum over sum of
  # digit product with frequency
  num = 0
  for digit in digits:
    num += (digit * freq)
  
  return digitSum(num)


if __name__ == "__main__":
  num, k = input().strip().split(' ')
  num, k = [int(num), int(k)]
  result = digitSum(num, k)
  print(result)